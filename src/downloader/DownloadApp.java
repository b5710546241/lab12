package downloader;

/**
 * Launch the URL downloader application.
 */
public class DownloadApp {

	/**
	 * Main method to start the user interface.
	 */
	public static void main(String[] args) {
		//TODO create the UI in the Event Dispatcher Thread.
		DownloaderUI ui = new DownloaderUI();
		ui.run();
	}
}
